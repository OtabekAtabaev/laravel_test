<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;
use Validator;
use Illuminate\Support\Facades\DB;

class SocialAuthGoogleController extends Controller
{
    /**
     * Redirect google auth
     */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * User check and login or registration
     */
    public function callback()
    {
        try {


            $googleUser = \Laravel\Socialite\Facades\Socialite::driver('google')->user();
            $existUser = User::where('email',$googleUser->email)->first();

            if($existUser) {
                //if user exist in db
                Auth::loginUsingId($existUser->id);
            }
            else {
                //create new user
                $user = new User;
                $user->first_name = $googleUser->user['name']['familyName'];
                $user->last_name = $googleUser->user['name']['givenName'];
                $user->email = $googleUser->email;
                $user->provider_id = $googleUser->id;
                $user->provider="google";
                $user->access_token=$googleUser->token;
                $user->password = "";
                $user->save();
                //login with insert id
                Auth::loginUsingId($user->id);
            }
           return redirect()->to('/home');
        }
        catch (Exception $e) {
            return 'error ';
        }
    }
}