@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Регистрация</h4></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="">
                          <label for="first_name" class="col-md-4 control-label">Имя</label>
                          <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="first_name" placeholder="Введите имя" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        {{ $errors->first('first_name') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>
                        <div class="">
                          <label for="last_name" class="col-md-4 control-label">Фамилия</label>
                          <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="last_name" placeholder="Введите фамилию" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        {{ $errors->first('last_name') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>
                        <div class="">
                          <label for="birth_day" class="col-md-4 control-label">День рождения</label>
                          <div class="form-group{{ $errors->has('birth_day') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="birth_day" placeholder="Введите день рождения" type="text" class="date form-control" name="birth_day" value="{{ old('birth_day') }}">

                                @if ($errors->has('birth_day'))
                                    <span class="help-block">
                                        {{ $errors->first('birth_day') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <label for="adress" class="col-md-4 control-label">Адрес</label>
                          <div class="form-group{{ $errors->has('adress') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="adress" placeholder="Введите адрес" type="text" class="form-control" name="adress" value="{{ old('adress') }}">

                                @if ($errors->has('adress'))
                                    <span class="help-block">
                                        {{ $errors->first('adress') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <label for="phone" class="col-md-4 control-label">Номер телефона</label>
                          <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="phone" placeholder="Введите номер телефона" type="text"  class="form-control inputmask" aria-invalid="false" aria-required="true"  data-mask="+999(99) 999-99-99" name="phone" value="{{ old('phone') }}" >

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        {{ $errors->first('phone') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <label for="email" class="col-md-4 control-label">Адрес электронной почты</label>
                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="email" placeholder="Введите адрес электронной почты" type="email" required oninvalid="this.setCustomValidity('Пожалуйста введите адрес электронной почты')" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>
                          </div>

                        </div>

                        <div class="">
                          <label for="password" class="col-md-4 control-label">Пароль</label>
                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="password" placeholder="Введите пароль" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <label for="password-confirm" class="col-md-4 control-label">Подтвердите Пароль</label>
                          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="password-confirm" placeholder="Подтвердите пароль" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif
                            </div>
                          </div>

                        </div>

                        <div class="">
                          <label for="file" class="col-md-4 control-label">Изображение профиля</label>
                          <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="file" style="color: transparent;" type="file" class="btn btn-ordinary" name="file" value="{{ old('file') }}">

                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        {{ $errors->first('file') }}
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    <i class="fa fa-btn"></i>Регистрироваться
                                </button>
                                <br>
                                <a href="{{ url('/login') }}" class="btn btn-warning btn-block"><i class="fa fa-btn fa-user"></i>Войти с именем пользователя</a>
                              
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        function setupViewJS() {
            $('.inputmask').inputmask({ mask: '+999(99)-999-99-99' });
            $('.date').datepicker({

                format: 'yyyy-mm-dd'

            });
        }
    </script>
@endsection
